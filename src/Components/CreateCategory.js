import React from 'react';

function CreateCategory() {
    return (
        <div className="main-container">
            <div className="row">
                <form>
                    <label>Create Category</label>
                    <input type="text" name="createCategory" />
                    <button>Add</button>
                </form>
            </div>
            <div className="row">

            </div>
        </div>
    )
}

export default CreateCategory;
