import React, { Component } from 'react';
import SidebarRouter from './Navigation/SidebarRouter';

export class App extends Component {
  render() {
    return (
      <div>
        <SidebarRouter />
      </div>
    )
  }
}

export default App;
