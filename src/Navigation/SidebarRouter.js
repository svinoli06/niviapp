import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Navbar from "./Navbar";
import Contactus from '../Pages/Contactus';
import Home from "../Pages/Home";
import Products from "../Pages/Products";

function SidebarRouter() {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/Products" exact component={Products}></Route>
          <Route path="/Contactus" exact component={Contactus}></Route>
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default SidebarRouter;
